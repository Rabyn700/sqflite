import 'package:flutter/material.dart';
import 'package:flutter_sqflite/models/places.dart';
import 'package:flutter_sqflite/screens/edit_place.dart';
import 'package:flutter_sqflite/screens/home_screen.dart';
import 'package:provider/provider.dart';
void main()=> runApp(Home());

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => Places(),
      child: MaterialApp(
        home: HomeScreen(),
        routes: {
          EditPlace.routeName: (context) => EditPlace(),
        },
      ),
    );
  }
}
