import 'package:flutter/material.dart';
import 'package:flutter_sqflite/models/places.dart';
import 'package:flutter_sqflite/screens/add_places.dart';
import 'package:flutter_sqflite/screens/edit_place.dart';
import 'package:provider/provider.dart';

//home screen
class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 40,
        backgroundColor: Colors.grey.withOpacity(0.10),
        title: Text('locally', style: TextStyle(color: Colors.black38),),),
      body: FutureBuilder(
        future: Provider.of<Places>(context, listen: false).fetchData(),
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(child:  CircularProgressIndicator(),);
          }
          return Consumer<Places>(
            child: Container(
                margin: const EdgeInsets.only(top: 10),
                padding: const EdgeInsets.all(15),
                child: Text('Add some places....', style: TextStyle(fontSize: 20),)),
            builder: (context, placeData, child) => placeData.items.length <=0 ? child :  ListView.builder(
                itemCount: placeData.items.length,
                itemBuilder: (context, index) => Card(
                  child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Column(
                      children: [
                        Text(placeData.items[index].title.toUpperCase(), style: TextStyle(fontSize: 18),),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [

                            SizedBox(height: 10,),
                            GestureDetector(
                                onTap: (){
                                  Navigator.pushNamed(context, EditPlace.routeName, arguments: placeData.items[index].id);
                                },
                                child: Image.file(placeData.items[index].image)),
                            RaisedButton.icon(
                                color: Colors.lightBlueAccent,
                                onPressed: (){
                                  showDialog(context: (context), builder: (context) => AlertDialog(
                                    title: Text('Are you sure ?'),
                                    content: Text('You want remove the item'),
                                    actions: [
                                      FlatButton(
                                        onPressed: (){
                                          Navigator.pop(context);
                                        },
                                        child: Text('No'),
                                      ),
                                      FlatButton(
                                        onPressed: (){
                                          Provider.of<Places>(context, listen: false).removeItem(placeData.items[index].id);
                                          Navigator.pop(context);
                                        },
                                        child: Text('Yes'),
                                      )
                                    ],
                                  ));

                            }, icon: Icon(Icons.delete), label: Text('remove', style: TextStyle(color: Colors.black54),))
                          ],
                        ),
                      ],
                    ),
                  ),
                )),
          );
        }
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
showModalBottomSheet(
    isScrollControlled: true,
    context: (context), builder: (context) => AddPlace());
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
