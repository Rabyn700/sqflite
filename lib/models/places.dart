import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_sqflite/helpers/databse_helper.dart';

class Place{
  final String title;
  final String id;
  final File image;

  Place({this.title, this.id, this.image});

}

class Places with ChangeNotifier{

  List<Place> _items = [];

  List<Place> get items{
    return [..._items];
  }

Place findById(String id){
    return _items.firstWhere((element) => element.id == id);
}
 void addPlace(String title, File image){

    final newPlace = Place(
      id: DateTime.now().toIso8601String(),
      title: title,
      image: image
    );
   _items.add(newPlace);
   notifyListeners();
DBHelper.insert('user_places', {
  'id': newPlace.id,
  'title': newPlace.title,
  'image': newPlace.image.path
});


 }

 void updateData(String id, Place newPlace){
    final proIndex = _items.indexWhere((element) => element.id == id);
    if(proIndex >= 0){
      _items[proIndex] = newPlace;
      notifyListeners();
    }else{
      print('...');
    }

    DBHelper.updateData('user_places', {
      'title': newPlace.title,
      'image': newPlace.image.path
    });

 }

 Future<void> fetchData() async{
final dataList = await DBHelper.getData('user_places');
_items = dataList.map((item) => Place(
  id: item['id'],
  title: item['title'],
  image: File(item['image']),
)).toList();
notifyListeners();
 }


  Future<void> removeItem(String id) async {

   _items.removeWhere((element) => element.id == id);
   notifyListeners();
    DBHelper.removeData('user_places', id);

  }


}