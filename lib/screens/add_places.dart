import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_sqflite/models/places.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
class AddPlace extends StatefulWidget {

  @override
  _AddPlaceState createState() => _AddPlaceState();
}

class _AddPlaceState extends State<AddPlace> {
  final titleController = TextEditingController();
  
  File imageFile;
  File savedImage;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 100,),
            TextField(
              controller: titleController,
              decoration: InputDecoration(
                labelText: 'Title'
              ),
            ),
            SizedBox(height: 50,),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 120,
                  width: 120,
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey)
                  ),
                  child: imageFile !=null ? Image.file(imageFile, fit: BoxFit.cover, height: 120, width: double.infinity,) : Text('add image', textAlign: TextAlign.center,),
                ),
                SizedBox(width: 20,),
                FlatButton.icon(
                    icon: Icon(Icons.camera),
                    onPressed: () async{
                      PickedFile image = await ImagePicker().getImage(source: ImageSource.gallery);
if(image == null){
  return;
}
setState(() {
  imageFile = File(image.path);
});
final appDir = await getApplicationDocumentsDirectory();
final fileName = basename(image.path);
savedImage = await File(image.path).copy('${appDir.path}/$fileName}');
                      
                    }, label: Text('Add a picture'))
              ],
            ),
            SizedBox(height: 20,),
            RaisedButton(onPressed: (){
              if(titleController.text.isEmpty || savedImage == null){
                return;
              }
              Provider.of<Places>(context, listen: false).addPlace(titleController.text, savedImage);
              Navigator.pop(context);
            }, child: Text('Submit'),)
          ],
        ),
      ),
    );
  }
}
