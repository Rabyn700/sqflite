

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_sqflite/models/places.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class EditPlace extends StatefulWidget {
  static const routeName = '/addPlace';
  @override
  _EditPlaceState createState() => _EditPlaceState();
}

class _EditPlaceState extends State<EditPlace> {
final _form = GlobalKey<FormState>();
  File imageFile;
  File savedImage;
bool _isInit = true;
String title = '';
Map _isInitValue = {
  'title': '',
  'image': null,
  'id': null
};

Place _place = Place();
  @override
  void didChangeDependencies() {
    if(_isInit) {
      final prodId = ModalRoute
          .of(context)
          .settings
          .arguments;
      if (prodId != null) {
        final place = Provider.of<Places>(context, listen: false).findById(
            prodId);
        _isInitValue = {
          'id': prodId,
          'title': place.title,
          'image': place.image
        };
        imageFile = place.image;
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Form(
            key: _form,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(height: 100,),
                TextFormField(
                  initialValue: _isInitValue['title'],
                  onSaved: (val){
                    title = val;
                  },
                  decoration: InputDecoration(
                      labelText: 'Title'
                  ),
                ),
                SizedBox(height: 50,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      height: 120,
                      width: 120,
                      decoration: BoxDecoration(
                          border: Border.all(width: 1, color: Colors.grey)
                      ),
                      child: imageFile !=null ? Image.file(imageFile, fit: BoxFit.cover, height: 120, width: double.infinity,) : Text('add image', textAlign: TextAlign.center,),
                    ),
                    SizedBox(width: 20,),
                    FlatButton.icon(
                        icon: Icon(Icons.camera),
                        onPressed: () async{
                          PickedFile image = await ImagePicker().getImage(source: ImageSource.gallery);
                          if(image == null){
                            return;
                          }
                          setState(() {
                            imageFile = File(image.path);
                          });
                          final appDir = await getApplicationDocumentsDirectory();
                          final fileName = path.basename(image.path);
                          savedImage = await File(image.path).copy('${appDir.path}/$fileName}');

                        }, label: Text('Add a picture'))
                  ],
                ),
                SizedBox(height: 20,),
                RaisedButton(onPressed: (){
               _form.currentState.save();
                  if(title.isEmpty ){
                    return;
                  }

                  _place = Place(
                    title: title,
                    image: savedImage == null ? imageFile : savedImage,
                    id: _isInitValue['id']
                  );
                   Provider.of<Places>(context, listen: false).updateData(_isInitValue['id'], _place);
                 Navigator.pop(context);
                }, child: Text('Submit'),)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
